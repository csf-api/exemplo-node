FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./


RUN npm install

COPY . .

#EXPOSE 80

CMD [ "npm", "start" ]

#docker build -t patrickmoro/exemplo-node:0.1 .

#docker images

#docker run -p 80:8081 -d --name exemplo-node1  patrickmoro/exemplo-node:0.1
