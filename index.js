'use strict';

const express = require('express');

let utilController = require('./controller/utilController');

const app = express();

app.get('/', (req, res) => {
  console.log('Acesso Negado');
  res.status(401).send();
});

app.get('/hello-world', (req, res) => {
  let retorno = {
    valor: 'get',
    mensagem: utilController.retornaValor(req.query.param1 || 'Sem_parametro_param1')
  };
  console.log('Get Hello world');
  res.status(200).send(retorno);
});

app.post('/hello-world', (req, res) => {
  console.log('Post Hello world');
  res.status(201).send();
});

app.get('/health', (req, res) => {
  console.log('health');
  res.status(200).send();
});

app.listen(process.env.PORT, process.env.HOST);

console.log(`Running on http://${process.env.HOST}:${process.env.PORT}/${process.env.CONFIG_MAP_CONTEXT}`);

module.exports = app;
