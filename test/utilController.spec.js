var assert = require('assert');
var request = require('supertest');
var utilController =  require('../controller/utilController');
var app = require('../index.js');

describe('utilController', function () {

  describe('retornaValor', function(){
    it('should return Concatena:Teste', function(){
      assert.equal(utilController.retornaValor('Teste'), 'Concatena:Teste');
    });
  });

  describe('GET /hello-world?param1=Teste', function() {
    it('respond with hello world', function(done) {
      request(app).get('/hello-world?param1=Teste').expect(200, done);
    });
  });

  describe('POST /hello-world', function() {
    it('respond with hello world', function(done) {
      request(app).post('/hello-world').expect(201, done);
    });
  });

});


